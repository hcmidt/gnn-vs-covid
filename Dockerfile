FROM python:3.8.5

WORKDIR /home

COPY gnncovid ./gnncovid
COPY test ./test
COPY recast ./recast
COPY scripts/random_regular_graph.py ./scripts/random_regular_graph.py
COPY requirements.txt ./requirements.txt
COPY setup.py ./setup.py
COPY README.md ./README.md

RUN pip install --no-cache-dir -r ./requirements.txt
RUN pip install --no-cache-dir  ./