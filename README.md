# GNN vs. COVID

## This repository

Is an exemplary implementation of a recastable analysis within the `RECAST` framework. The project itself just serves as a dummy workflow. For details on the recast-part, cf. [recast](./recast). In order to run the analysis within the recast do the following (assuming you have the necessary dependencies installed and cloned this repository):

```
cd recast
$(recast catalogue add $PWD)
recast run gnncovid/rrg --backend docker
```

### Install 

Either install locally with
```
pip install .
```
or use the `Dockerfile`


## The project

Is work in progress. The idea is simple enough:

1. Use a graph neural network (GNN) to learn the dynamics of an infection spreading on a graph. Here we use threshold dynamics to model the spread of an infection on the graph (technically we use the dual problem, but that is just a detail).
2. The learning is done by massage passing in a supervised way. We input a graph, an initial infection and the resulting final state of infections (i.e. the state that is reached by the dynamics as a steady state). The GNN is trained to predict the final state, given the initial state and the graph structure.
3. Once these dynamics are `understood' by the GNN it provides us with a hidden representation of the current state of the infection on the graph. These representations are vectors that live on the nodes of the graph.
4. In a final step we foster this representation in order to learn the best strategy to intervene in the process in order to stop the infection from spreading. This is done by reinforcement learning.

## Currently implemented on master

- [x] prediction of infection spreading
- [x] recast
- [ ] control infection by vaccination
- [ ] docs

## Currently under development

- The control part that aims to find best strategy. 
- First results are obtained: works better than random benchmark strategies, but does not reach optimal performance yet.
- A recast analysis will be made available soon
