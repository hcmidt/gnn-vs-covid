import collections
import numpy as np
import networkx as nx


NestedInput = collections.namedtuple('NestedInput', ['adj', 'ini', 'fin'])


def _make_instance(n, degree, kc):
    # make graph
    G = nx.random_regular_graph(degree, n)
    # make adjacency list
    A = np.array([list(G.neighbors(i)) for i in range(n)], dtype=np.int32)
    # random initial removal
    r = np.random.randint(n)
    nums = np.arange(n)
    rm = np.random.choice(nums, r, replace=False)
    x_0 = np.ones(n)
    x_0[rm] = 0
    G.remove_nodes_from(rm)
    # compute k-Core
    C = nx.k_core(G, kc)
    x_oo = np.zeros(n)
    x_oo[list(C.nodes)] = 1
    x_0 = x_0[:, np.newaxis]
    x_oo = x_oo[:, np.newaxis]
    return A, x_0, x_oo


def make_full_instance(T, n, degree, kc):
    # want shape (timesteps, batch_size, ...)
    A, x_0, x_oo = _make_instance(n, degree, kc)
    As, x_0s, x_oos = [A]*T, [x_0]*T, [x_oo]*T
    return NestedInput(adj=np.array(As, dtype=np.int32),
                       ini=np.array(x_0s, dtype=np.float32),
                       fin=np.array(x_oos, dtype=np.float32))