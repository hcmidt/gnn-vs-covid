import tensorflow as tf
import tensorflow.keras.backend as K

from tensorflow.keras import layers


class rrgCell(layers.Layer):

    def __init__(self,
                 n,
                 dim_f,
                 units=None,
                 eps=1,
                 **kwargs
                 ):
        super(rrgCell, self).__init__(**kwargs)
        self.n = n
        self.dim_f = dim_f
        if not units:
            self.units = dim_f // 2
        else:
            self.units = units
        self.eps = eps
        
        self.state_size = (1, dim_f)
        self.output_size = (1, 1, n, n)
        
    def propagator(self, x):
        # first layer
        h = K.dot(x, self.pk1)
        h = K.bias_add(h, self.pb1)
        h = tf.keras.activations.relu(h)
        # second layer
        h = K.dot(h, self.pk2)
        h = K.bias_add(h, self.pb2)
        y = tf.keras.activations.relu(h)
        return y
    
    def energy(self, x):
        # first layer
        h = K.dot(x, self.ek1)
        h = K.bias_add(h, self.eb1)
        h = tf.keras.activations.relu(h)
        # second layer
        h = K.dot(h, self.ek2)
        h = K.bias_add(h, self.eb2)
        y = tf.keras.activations.relu(h)
        # third layer
        h = K.dot(h, self.ek3)
        y = K.bias_add(h, self.eb3)
        return y
    
    def predictor(self, x):
        # first
        h = K.dot(x, self.tk1)
        h = K.bias_add(h, self.tb1)
        h = tf.keras.activations.relu(h)
        # second layer
        h = K.dot(h, self.tk2)
        h = K.bias_add(h, self.tb2)
        y = tf.keras.activations.relu(h)
        # third layer
        h = K.dot(h, self.tk3)
        h = K.bias_add(h, self.tb3)
        y = tf.keras.activations.sigmoid(h)
        return y
        
    def build(self, input_shapes):
        # propagator
        self.pk1 = self.add_weight(
            shape=(input_shapes[0][-1], self.units),
            name='first_kernel')
        self.pk2 = self.add_weight(
            shape=(self.units, self.dim_f),
            name='second_kernel')
        self.pb1 = self.add_weight(
            shape=(self.units,),
            name='first_bias')
        self.pb2 = self.add_weight(
            shape=(self.dim_f,),
            name='second_bias')
        # predictor
        self.tk1 = self.add_weight(
            shape=(input_shapes[1][-1], input_shapes[1][-1]//2),
            name='first_kernel')
        self.tk2 = self.add_weight(
            shape=(input_shapes[1][-1]//2, input_shapes[1][-1]//4),
            name='second_kernel')
        self.tk3 = self.add_weight(
            shape=(input_shapes[1][-1]//4, 1),
            name='third_kernel')
        self.tb1 = self.add_weight(
            shape=(input_shapes[1][-1]//2,),
            name='first_bias')
        self.tb2 = self.add_weight(
            shape=(input_shapes[1][-1]//4,),
            name='second_bias')
        self.tb3 = self.add_weight(
            shape=(1,),
            name='third_bias')
        # energy
        self.ek1 = self.add_weight(
            shape=(input_shapes[2][-1], input_shapes[2][-1]//2),
            name='first_kernel')
        self.ek2 = self.add_weight(
            shape=(input_shapes[2][-1]//2, input_shapes[2][-1]//4),
            name='second_kernel')
        self.ek3 = self.add_weight(
            shape=(input_shapes[2][-1]//4, 1),
            name='third_kernel')
        self.eb1 = self.add_weight(
            shape=(input_shapes[2][-1]//2,),
            name='first_bias')
        self.eb2 = self.add_weight(
            shape=(input_shapes[2][-1]//4,),
            name='second_bias')
        self.eb3 = self.add_weight(
            shape=(1,),
            name='third_bias')

        self.built = True

    def call(self, inputs, states):
        # inputs: graph adj, activation_at_t, activation_at_{t+1} (#nodes, #nodes)
        # we use: graph adj, activation_at_0, activation_at_oo (#nodes, #nodes)
        # states : (#dim_f)
        # (#nodes, dim_f+1)
        A, x_0, x_oo = tf.nest.flatten(inputs)
        # construct features from input and states
        pred, hidden = states
        nbs_hidden = self._accumulate(hidden, A)
        features = tf.concat([x_0, hidden, nbs_hidden], -1)
        # propagate features => new state
        new_hidden = self.propagator(features)
        # compute prediction
        pred_features = tf.concat([pred, new_hidden], -1)
        new_pred = self.predictor(pred_features)
        # estimate energy
        energy_features = tf.concat([new_pred, new_hidden], -1)
        # energy_features_feed = energy_features
        energy_features_feed = tf.stop_gradient(energy_features)
        estimated_energy = self.energy(energy_features_feed)
        # compute loss of prediction
        pred_loss_t = self.pred_loss_fun(x_oo, new_pred)
        energy_loss = self.energy_loss_fun(x_0, x_oo, estimated_energy)
        # output loss and prediction
        output = [pred_loss_t, energy_loss, new_pred, estimated_energy]
        return output, [new_pred, new_hidden]
    
    def energy_loss_fun(self, x_0, x_oo, estimated_energy):
        true_energy = tf.reduce_sum(x_0, keepdims=True) - self.eps * tf.reduce_sum(1-x_oo, keepdims=True)
        loss = tf.math.squared_difference(estimated_energy, true_energy)
        return loss
    
    def pred_loss_fun(self, y_true, y_hat):
        local_loss = K.binary_crossentropy(y_true, y_hat)
        loss = local_loss
        # loss = tf.reduce_sum(local_loss, axis=-1, keepdims=True)
        return loss
    
    def _accumulate(self, state, A):
        # accumulate states according to nbs
        # state is (n, dim_f)
        # accumulates state sums over incoming edges
        nbs_states = tf.gather(state, A)
        acc_state = tf.reduce_sum(nbs_states , axis=1)
        return acc_state
    
    def get_initial_state(self, inputs=None, batch_size=None, dtype=None):
        # make uniform features
        ini_pred = tf.random.uniform((self.n, 1), dtype=tf.float32)
        ini_hidden = tf.random.uniform((self.n, self.dim_f), dtype=tf.float32)
        return [ini_pred, ini_hidden]