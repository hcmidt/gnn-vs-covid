import collections
import tensorflow as tf
import tensorflow.keras.backend as K

from tensorflow.keras import layers


class kCoreModel(tf.keras.Model):

    def __init__(self,
                 rnn_cell,
                 learning_rate=1e-3,
                 **kwargs,
                 ):
        super(kCoreModel, self).__init__(**kwargs)
        self.learning_rate = learning_rate
        # shape of propagator, shape of predictor
        self.rnn_cell = rnn_cell
        self.nn_shapes = [(rnn_cell.n, 2*rnn_cell.dim_f+1),
                          (rnn_cell.n, rnn_cell.dim_f+1),
                          (rnn_cell.n, rnn_cell.dim_f+1)]
        self.rnn_cell.build(self.nn_shapes)
        self.rnn = tf.keras.layers.RNN(self.rnn_cell, time_major=True)

        self.optimizer = tf.keras.optimizers.Adam(self.learning_rate)

    @tf.function
    def call(self, inputs, training=False):
        losses, energy_losses, preds, energies = self.rnn(inputs)
        pred_loss = tf.reduce_sum(losses)
        energy_loss = tf.reduce_sum(energy_losses)
        if training:
            return pred_loss, energy_loss
        else:
            return preds, energies

    @tf.function
    def predict(self, x,):
        x = tf.dtypes.cast(x, dtype=tf.float32)
        return self.rnn_cell.predictor(x)

    @tf.function
    def train(self, train_dataset):
        # train_datset is list of inputs of shape (length, batch_size, dim)
        losses, n_chunks = [], len(train_dataset)
        for i, inputs in enumerate(train_dataset):
            with tf.GradientTape() as tape:
                pred_loss, energy_loss = self.call(inputs, training=True)
                loss_value = pred_loss + energy_loss
                losses.append(loss_value)
            grads = tape.gradient(loss_value, self.trainable_weights)
            self.optimizer.apply_gradients(zip(grads, self.trainable_weights))
        tf.print('Training done.')
        return losses