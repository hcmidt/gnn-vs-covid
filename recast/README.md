In order to re-run the analysis locally
```
$(recast catalogue add $PWD)
recast run gnncovid/rrg --backend docker
```

Currently the result is a random variable and should not be expected to look the same for every run.
However, it should be somewhat similar. The randomness can also be fixed, but this just a proof of concept to show the recast workflow.
