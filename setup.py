# -----------------------------------------------------------------------------
# setup.py
#
# project:  gnn_vs_covid
# author:   hcmidt
# created:  2021-03-01
#
# -----------------------------------------------------------------------------


import os
import re

from setuptools import setup, find_packages


def get_requirements(filename='requirements.txt'):
    ret = []
    if os.path.isfile(filename):
        for x in open(filename):
            ret.append(x.strip())
    return ret


def get_version(dir_name):
    for x in open(os.path.join(dir_name, '__init__.py')):
        x = [y.strip() for y in x.split('=', 1)]
        if len(x) == 2 and x[0] == '__version__':
            match = re.match(r"^['\"](\d+\.\d+\.\d+\w*)['\"]", x[1].strip())
            if match:
                return match.group(1)
    raise ValueError('__version__ not found in __init__.py')


def get_scripts(dir_name):
    ret = []
    if os.path.isdir(dir_name):
        for fn in os.listdir(dir_name):
            ret.append(os.path.join(dir_name, fn))
    return ret


def get_data(dir_name):
    ret = []
    if os.path.isdir(dir_name):
        for d, a, l in os.walk(dir_name):
            ret.append((d, [os.path.join(d, x) for x in l]))
    return ret


setup(
    name='gnncovid',
    version=get_version('gnncovid'),
    author='Hinnerk Christian schmidt',
    author_email='hinnerkschmidt@gmail.com',
    maintainer='Hinnerk Christian Schmidt',
    maintainer_email='hinnerkschmidt@gmail.com',
    packages=find_packages('.', exclude=('tests', 'docs', 'build')),
    description='Use gnns to control infection from spreading on graph.',
    install_requires=get_requirements(),
    license='2021 hinnerk christian schmidt',
)