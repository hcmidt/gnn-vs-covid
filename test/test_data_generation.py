import pytest
import numpy as np


testdata = list(zip(np.random.randint(1, 100, size=10), np.random.choice(np.arange(10, 100, 2), size=10), np.random.randint(1, 10, size=10), np.random.randint(1, 10, size=10)))
@pytest.mark.parametrize("T, n, d, k", testdata)
def test_prediction_data(T, n, d, k):
    from gnncovid.utils.data import make_full_instance
    data = make_full_instance(T, n, d, k)
    assert data.adj.shape == (T, n, d)
    assert data.ini.shape == (T, n, 1)
    assert data.fin.shape == (T, n, 1)
    assert all([np.all(data.adj[0] == data.adj[i]) for i in range(T)])
    assert all([np.all(data.ini[0] == data.ini[i]) for i in range(T)])
    assert all([np.all(data.fin[0] == data.fin[i]) for i in range(T)])
    assert np.sum(data.ini[0]) >= np.sum(data.fin[0])