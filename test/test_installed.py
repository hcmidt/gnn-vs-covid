def test_if_installed():
    try:
        import gnncovid as gc
        from gnncovid.models import kCoreModel
        from gnncovid.cells import rrgCell
        from gnncovid.utils.data import make_full_instance
        assert kCoreModel is not None
        assert rrgCell is not None
        assert make_full_instance is not None
        assert gc is not None
    except Exception as e:
        raise e


def test_requirements():
    try:
        import matplotlib.pyplot as plt
        import networkx as nx
        import numpy as np
        import tensorflow as tf
    except Exception as e:
        raise e