import pytest
import numpy as np


testdata = list(zip(np.random.randint(1, 100, size=10), np.random.randint(1, 100, size=10)))


@pytest.mark.parametrize("n, dim_f", testdata)
def test_cell_init(n, dim_f):
    from gnncovid.cells import rrgCell
    cell = rrgCell(n, dim_f=dim_f)
    assert cell.n == n
    assert cell.dim_f == dim_f
    assert cell.state_size == (1, dim_f)
    assert cell.output_size == (1, 1, n, n)
    assert cell.units == dim_f // 2


@pytest.mark.parametrize("n, dim_f", testdata)
def test_cell_build(n, dim_f):
    from gnncovid.cells import rrgCell
    cell = rrgCell(n, dim_f=dim_f)
    nn_shapes = [(cell.n, 2*cell.dim_f+1),
                 (cell.n, cell.dim_f+1),
                 (cell.n, cell.dim_f+1)]
    cell.build(nn_shapes)
    assert cell.built == True
    assert len(cell.trainable_variables) == 16


@pytest.mark.parametrize("n, dim_f", testdata)
def test_cell_nns(n, dim_f):
    from gnncovid.cells import rrgCell
    import tensorflow as tf
    cell = rrgCell(n, dim_f=dim_f)
    nn_shapes = [(cell.n, 2*cell.dim_f+1),
                 (cell.n, cell.dim_f+1),
                 (cell.n, cell.dim_f+1)]
    cell.build(nn_shapes)
    x = tf.random.uniform((n, 2*dim_f+1), dtype=tf.float32)
    y = cell.propagator(x)
    assert y.shape == (n, dim_f)
    x = tf.random.uniform((n, dim_f+1))
    y = cell.predictor(x)
    assert y.shape == (n, 1)
    x = tf.random.uniform((n, dim_f+1))
    y = cell.energy(x)
    assert y.shape == (n, 1)


def test_cell_nn_fail(n=100, dim_f=32):
    from gnncovid.cells import rrgCell
    cell = rrgCell(n, dim_f=dim_f)
    nn_shapes = [(cell.n, 2*cell.dim_f+1),
                 (cell.n, cell.dim_f+1),
                 (cell.n, cell.dim_f+1)]
    x = np.random.randn(n, 2*dim_f+1)
    try:
        y = cell.predictor(x)
    except AttributeError as e:
        assert True