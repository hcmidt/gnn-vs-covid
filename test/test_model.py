import pytest
import numpy as np


testdata = list(zip(np.random.choice(np.arange(10, 100, 2), size=10), np.random.randint(1, 100, size=10)))


@pytest.mark.parametrize("n, dim_f", testdata)
def test_model(n, dim_f):
    T = int(np.log(n))
    d = np.random.randint(2, n//2)
    k = np.random.randint(1, max(d, 2))
    from gnncovid.models import kCoreModel
    from gnncovid.cells import rrgCell
    from gnncovid.utils.data import make_full_instance
    cell = rrgCell(n, dim_f)
    model = kCoreModel(rnn_cell=cell)
    data = make_full_instance(T, n, d, k)
    loss1, loss2 = model.call(data, training=True)
    assert loss1.shape == loss2.shape == ()
    x, y = model.call(data, training=False)
    assert x.shape == y.shape == (n, 1)


def test_train():
    from gnncovid.models import kCoreModel
    from gnncovid.cells import rrgCell
    from gnncovid.utils.data import make_full_instance
    n = 100
    T = int(np.log(n))
    d = 3
    k = 2
    cell = rrgCell(n, dim_f=32)
    model = kCoreModel(rnn_cell=cell)
    data = make_full_instance(T, n, d, k)
    l1, l2 = model.call(data, training=True)
    loss0 = l1 + l2
    _ = model.train([data])
    l1, l2 = model.call(data, training=True)
    loss1 = l1 + l2
    assert loss1.shape == loss0.shape
