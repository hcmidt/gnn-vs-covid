import argparse
import logging

import numpy as np
import matplotlib.pyplot as plt
import tensorflow.keras.backend as K

from gnncovid.models import kCoreModel
from gnncovid.cells import rrgCell
from gnncovid.utils.data import make_full_instance


def runner(fname):
    # define problem
    n = 100  # size of network
    degree = 3  # degree for random regular graphs
    kc = 2  # k for the k-core

    # gnn training variables
    m = 100  # the number of samples (graphs)
    T = int(np.log(n))  # the number of message passing steps
    epochs = 100

    # define model
    cell = rrgCell(n, dim_f=32)
    model = kCoreModel(rnn_cell=cell)

    # make training data
    train_set = [make_full_instance(T, n, degree, kc) for i in range(m)]
    test_set = [make_full_instance(T, n, degree, kc) for i in range(m)]
    
    # iterate several times over training data
    col_loss = []
    for epoch in range(epochs):
        logging.info('##### Epoch %d / %d #####'%(epoch+1, epochs))
        loss = model.train(train_set)
        col_loss.append(np.mean([el.numpy() for el in loss])/n)

    # compute some observables
    calls = [model.call(test_set[idx]) for idx in range(m)]
    preds = np.array([np.mean((calls[idx][0].numpy() > 0.5).astype(np.float32)) for idx in range(m)])
    truths = np.array([np.mean(test_set[idx].fin[0]) for idx in range(m)])
    inis = np.array([np.mean(test_set[idx].ini[0]) for idx in range(m)])
    entropies = np.array([K.binary_crossentropy(test_set[idx].fin[0], calls[idx][0]).numpy().mean()
                          for idx in range(m)])
    accs = np.array([((calls[idx][0].numpy() > 0.5).astype(np.float32) == test_set[idx].fin[0]).mean()
                   for idx in range(m)])
    energies = np.array([calls[idx][1].numpy().mean() for idx in range(m)])
    true_energies = np.array([(test_set[idx].ini[0]-model.rnn_cell.eps*(1-test_set[idx].fin[0])).sum()
                              for idx in range(m)])

    # generate output and write to file
    plt.figure(figsize=(8, 5))
    plt.scatter(1-inis, truths, c='k', alpha=0.5, label='truth')
    plt.scatter(1-inis, preds, c='r', alpha=0.5, label='prediction')
    plt.xlabel('fraction of initially vaccinated')
    plt.ylabel('fraction of final infections')
    plt.legend()
    plt.title('Truth vs. GNN prediction')
    plt.savefig(fname)

    return preds


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('fname')
    args = parser.parse_args()
    fname = args.fname
    preds = runner(fname)
    print(preds)